# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  if arr == arr.sort
    return true
  else
    return false
  end
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  letter_arr = str.downcase.split("")
  vowel_arr = letter_arr.select { |l| l =~ /[aeiou]/ }
  vowel_arr.length
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  letter_arr = str.split("")
  consonant_array = letter_arr.select{ |l| ["a", "e", "i", "o", "u"].include?(l.downcase) == false }
  consonant_array.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  digit_arr = int.to_s.split("")
  digit_arr.map!(&:to_i)
  digit_arr.sort.reverse.map!(&:to_s)
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  0.upto(str.length - 2) do |idx1|
    (idx1 + 1).upto(str.length - 1) do |idx2|
      if str[idx1].downcase == str[idx2].downcase
        return true
      end
    end
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0..2].join}) #{arr[3..5].join}-#{arr[6..9].join}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  range(str.split(",").map!(&:to_i))
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!


def my_rotate(arr, offset=1)
  arr_length = arr.length

  result =
    if offset.abs % arr_length == 0
      arr
    elsif offset > 0
      true_offset = offset % arr_length
      arr.drop(true_offset) + arr.take(true_offset)
    elsif offset < 0
      true_offset = (-1) * (offset.abs % arr_length)
      segment = arr_length + true_offset
      arr.drop(segment) + arr.take(segment)
    end

  result
end

puts my_rotate(["a", "b", "c", "d"], -3)
